package com.cy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@FeignClient(name = "nacos-provider") //@FeignClient内部为服务名
@RestController
interface ConsumerHttpApi{
    //假如启动时出现 PathVariable annotation was empty on param 0.这个错误
    //在需要在@PathVariable注解中主子定value属性的值
    @GetMapping("/provider/echo/{string}")
    String echoMessage(@PathVariable(value = "string") String string);
}

@RestController
@RequestMapping("/consumer/feign/")
public class FeignConsumerController {

    @Value("${spring.application.name}")
    private String appName;

    @Autowired
    private ConsumerHttpApi consumerHttpApi;

    @GetMapping("{str}")
    public String doFeignEcho(@PathVariable String str){
        return consumerHttpApi.echoMessage(str);
    }

    /**基于feign方式的服务调用*/
    @GetMapping
    public String doFeignEcho(){
        return consumerHttpApi.echoMessage(appName);
    }



}
