package com.cy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class NacosConfigApplication {

    private static final Logger log
            = LoggerFactory.getLogger(NacosConfigApplication.class);

    @RefreshScope //支持配置动态刷新
    @RestController
    @RequestMapping("/config/")
    public class NacosConfigController{

        //@Value是读取配置信息内容
        @Value("${logging.level.com.cy:info}")//若取不到，默认值是info
        private String logLevel;

        @Value("${server.tomcat.thread.max:128}")
        private Integer maxThread;

        @GetMapping("doGetMaxThread")
        public Integer doGetMaxThread(){
            return maxThread;
        }

        @Value("${page.pageSize:10}")
        private int PageSize;

        @GetMapping("doGetPageSize")
        public Integer doGetPageSize(){
            return  PageSize;
        }

        @GetMapping("doGetLogLevel")
        public String doGetLogLevel(){
//          System.out.println("fuck");
            log.debug("debug info");
            log.error("error info");
            return logLevel;
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(NacosConfigApplication.class,args);
    }
}
