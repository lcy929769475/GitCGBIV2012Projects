package com.cy;

import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JsonbTester;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class StringRedisTemplateTests {

    /**此对象是为spring提供的一个用于操作Redis数据库的字符串的一个对象*/
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void testOpsForValueSet(){
        stringRedisTemplate.opsForValue().set("lage", "18");
        stringRedisTemplate.opsForValue().set("id", "111", 1000, TimeUnit.SECONDS);
        System.out.println("set ok");
//        testOpsForValueGet();
        Map<String,String> map =new HashMap<>();
        map.put("id", "100");
        map.put("title", "hello redis");
        map.put("content", "redis is very good~");
        String jsonStr = JSON.toJSONString(map);
        stringRedisTemplate.opsForValue().set("message",jsonStr);
    }

    @Test
    void testOpsForValueGet(){
        String name = stringRedisTemplate.opsForValue().get("name");
        System.out.println("name"+name);
        String age = stringRedisTemplate.opsForValue().get("lage");
        System.out.println("lage"+age);
        String id = stringRedisTemplate.opsForValue().get("id");
        System.out.println("lage"+id);
        String message = stringRedisTemplate.opsForValue().get("message");
        Map<String,String> map=( Map<String,String>)JSON.parse(message);
        System.out.println(map);

    }
}
