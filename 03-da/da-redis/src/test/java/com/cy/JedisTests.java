package com.cy;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class JedisTests {
    @Test
    void testRedisShard(){
        //Jedis连接池的配置
        JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(200);
        //Jedis中的具体分片对象
        JedisShardInfo info1=new JedisShardInfo("192.168.233.130", 6379);
        JedisShardInfo info2=new JedisShardInfo("192.168.233.130", 6380);
        List<JedisShardInfo> jedisShardInfos=new ArrayList<>();
        jedisShardInfos.add(info1);
        jedisShardInfos.add(info2);

        //redis分片关键对象ShardedJedisPool
        ShardedJedisPool shardedJedisPool=
                new ShardedJedisPool(jedisPoolConfig,jedisShardInfos);
        for(int i=0;i<10;i++){
            ShardedJedis resource = shardedJedisPool.getResource();
            //将name+i做为key,i对应的字符串作为value写入到redis中
            resource.set("name"+i,String.valueOf(i));
        }
        shardedJedisPool.close();

    }
    @Test
    void testGet6379()throws Exception{
        Jedis jedis=new Jedis("192.168.233.130", 6379);
        String name=jedis.get("name");
        System.out.println("jedis.name="+name);
    }
    @Test
    void testGet6380()throws Exception{
        Jedis jedis=new Jedis("192.168.233.130", 6380);
        String name=jedis.get("name");
        System.out.println("jedis.name="+name);
    }
}
