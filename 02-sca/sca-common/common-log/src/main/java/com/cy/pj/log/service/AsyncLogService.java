package com.cy.pj.log.service;

import com.cy.pj.log.pojo.SysLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncLogService {//异步日志service

    @Autowired
    private RemoteLogService remoteLogService;//feign

//    public AsyncLogService(){
//        this.remoteLogService=remoteLogService;
//    }


    @Async //异步调用远端日志服务
    public void saveLog(SysLog log){
        remoteLogService.saveLog(log);
    }
}
