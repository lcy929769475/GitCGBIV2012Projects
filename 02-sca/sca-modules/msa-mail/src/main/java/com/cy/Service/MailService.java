package com.cy.Service;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.cy.Dao.Mail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;

@Slf4j
@Service
public class MailService {

    @Autowired
    private JavaMailSenderImpl mailSender;

    public Mail sendMail(Mail mail) {
        try {
            checkMail(mail);
            sendMimeMail(mail);
            return saveMail(mail);
        } catch (Exception e) {
            log.error("发送失败", e);
            mail.setStatus("fail");
            mail.setError(e.getMessage());
            return mail;
        }

    }

    private void checkMail(Mail mail) {
        if (StringUtils.isEmpty(mail.getTo())) {
            throw new RuntimeException("邮件收信人不能为空");
        }
        if (StringUtils.isEmpty(mail.getSubject())) {
            throw new RuntimeException("邮件主题不能为空");
        }
        if (StringUtils.isEmpty(mail.getText())) {
            throw new RuntimeException("邮件内容不能为空");
        }


    }


    private void sendMimeMail(Mail mail) throws MessagingException {
        try {
        MimeMessageHelper messageHelper = new MimeMessageHelper(mailSender.createMimeMessage(), true);
        mail.setFrom(getMailSendFrom());
        messageHelper.setFrom(mail.getFrom());
        messageHelper.setTo(mail.getTo().split(","));
        messageHelper.setSubject(mail.getSubject());
        messageHelper.setText(mail.getText());
        if (!StringUtils.isEmpty(mail.getCc())) {
            messageHelper.setCc(mail.getCc().split(","));
        }
        if (!StringUtils.isEmpty(mail.getBcc())) {
            messageHelper.setCc(mail.getBcc().split(","));
        }
        if (mail.getMultipartFiles() != null) {
            for (MultipartFile multipartFile : mail.getMultipartFiles()) {
                messageHelper.addAttachment(multipartFile.getOriginalFilename(), multipartFile);
            }
        }
//        if (StringUtils.isEmpty(mail.getSentDate())) {
//            mail.setSentDate(new Date());
//            messageHelper.setSentDate(mail.getSentDate());
//        }

        mailSender.send(messageHelper.getMimeMessage());
        mail.setStatus("ok");
        log.info("发送邮件成功：{}->{}", mail.getFrom(), mail.getTo());
    } catch (Exception e) {
        throw new RuntimeException(e);
    }

    }


    private Mail saveMail(Mail mail) {

        return mail;
    }

    public String getMailSendFrom() {
        return mailSender.getJavaMailProperties().getProperty("from");
    }

}
